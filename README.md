# Uter vue-app

## Heroku live version
Try the Live Version -> https://vue-client-uter.herokuapp.com/

_When an app on Heroku doesn't receive any traffic in 1 hour, the app goes to sleep._
_This causes a short delay for this first request, but subsequent requests will perform normally._


## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
