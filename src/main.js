import Vue from 'vue'
import App from './App.vue'
import router from './router';
import { Datetime } from 'vue-datetime'
import 'vue-datetime/dist/vue-datetime.css'
import Vuelidate from 'vuelidate'

import './assets/css/main.css';
import { BootstrapVue } from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

Vue.use(Datetime)
Vue.use(require('vue-moment'))
Vue.use(Vuelidate)
Vue.use(BootstrapVue)

Vue.config.productionTip = false
// Vue.prototype.$hostname = 'http://localhost:8080'
Vue.prototype.$hostname = 'https://spring-server-uter.herokuapp.com'

new Vue({
  router,
  render: h => h(App),
}).$mount('#app')
