import Vue from "vue";
import Router from "vue-router";

Vue.use(Router);

export default new Router({
    mode: 'history',
    routes: [
        {
            name: "trip",
            path: "/trip",
            alias: "/",
            component: () => import("@/views/Trip")
        },
        {
            name: "vehicle",
            path: "/vehicle",
            component: () => import("@/views/Vehicle")
        },
        {
            name: "driver",
            path: "/driver",
            component: () => import("@/views/Driver")
        }
    ]
})